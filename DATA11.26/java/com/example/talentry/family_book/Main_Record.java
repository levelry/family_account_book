package com.example.talentry.family_book;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TalentRY on 2018/11/21.
 */

public class Main_Record extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout title;
    private RelativeLayout back;
    private TextView expenditure, income;
    private View view1, view2;
    private ViewPager content;
    private List<Fragment> fragmentList;
    private Expenditure_Fragment expenditure_fragment;/*支出*/
    private Income_Fragment income_fragment;/*收入*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);
        findIDs();
        setListeners();
        defaultFragment();
    }


    private void findIDs() {
        title = (RelativeLayout) findViewById(R.id.title);
        back = (RelativeLayout) findViewById(R.id.back);
        expenditure = (TextView) findViewById(R.id.expenditure);
        income = (TextView) findViewById(R.id.income);

        view1 = findViewById(R.id.view1);
        view2 = findViewById(R.id.view2);

    }

    private void setListeners() {
        back.setOnClickListener(this);
        expenditure.setOnClickListener(this);
        income.setOnClickListener(this);
    }


    private void defaultFragment() {
        content = (ViewPager) findViewById(R.id.content);
        fragmentList = new ArrayList<>();
        fragmentList.add(new Expenditure_Fragment());
        fragmentList.add(new Income_Fragment());
        /*  加载Fragment适配器  */
        Fragment_adapter adapter = new Fragment_adapter(getSupportFragmentManager(), fragmentList);
        content.setAdapter(adapter);
        view1.setBackgroundColor(0xff5AC994);
        title.setBackgroundColor(0xff5AC994);
        content.setCurrentItem(0);
        content.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                reset();
                if (position == 0) {
                    view1.setBackgroundColor(0xff5AC994);
                    title.setBackgroundColor(0xff5AC994);
                }
                if (position == 1) {
                    view2.setBackgroundColor(0xff29AAFB);
                    title.setBackgroundColor(0xff29AAFB);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.expenditure:
                if (expenditure_fragment == null) {
                    view1.setBackgroundColor(0xff5AC994);
                    title.setBackgroundColor(0xff5AC994);
                    content.setCurrentItem(0);
                    return;
                }
                break;
            case R.id.income:
                if (income_fragment == null) {
                    view2.setBackgroundColor(0xff29AAFB);
                    title.setBackgroundColor(0xff29AAFB);
                    content.setCurrentItem(1);
                    return;
                }
                break;
        }
    }


    /*提示条*/
    private void reset() {
        view1.setBackgroundColor(0xffFFFFFF);
        view2.setBackgroundColor(0xffFFFFFF);
    }


}
