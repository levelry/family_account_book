package com.example.talentry.family_book;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import static com.example.talentry.family_book.A.api;

/**
 * Created by TalentRY on 2018/11/21.
 */

public class Main extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout settings, home;
    private TextView expenditure, income;
    private TextView charge_up;
    private List<List_Contact> data;
    private RecyclerView recyclerview;
    public static String family;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findIDs();
        setListeners();
        setData();

    }


    private void findIDs() {
        settings = (RelativeLayout) findViewById(R.id.settings);
        home = (RelativeLayout) findViewById(R.id.home);
        charge_up = (TextView) findViewById(R.id.charge_up);
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        expenditure = (TextView) findViewById(R.id.expenditure);
        income = (TextView) findViewById(R.id.income);
    }

    private void setListeners() {
        settings.setOnClickListener(this);
        home.setOnClickListener(this);
        charge_up.setOnClickListener(this);
    }


    /*public static String username;

    public String cook(){
        SharedPreferences pref = getSharedPreferences("user",MODE_PRIVATE);
        username = pref.getString("name","");
        return username;
    }*/


    private void setData() {
        SharedPreferences pref = getSharedPreferences("user", MODE_PRIVATE);
        String username = pref.getString("name", "");

        String zhichu = parseJSONWithJSONObject(api("http://139.199.70.192/zb/get_date_he_user.php?user=" + username + "&money=1"));
        String shouru = parseJSONWithJSONObject(api("http://139.199.70.192/zb/get_date_he_user.php?user=" + username + "&money=0"));
        expenditure.setText(zhichu);
        income.setText(shouru);


        List_Contacts contacts = new List_Contacts();
        data = contacts.getContacts();

        List_Adapter adapter = new List_Adapter(data, this);
        recyclerview.setAdapter(adapter);


        LinearLayoutManager lin = new LinearLayoutManager(this);
        lin.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview.setLayoutManager(lin);


        /*recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            public LinearLayoutManager linearLayoutManager;
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == RecyclerView.SCROLL_STATE_IDLE){
                    int lastVisiblePosition = linearLayoutManager.findLastVisibleItemPosition();
                    if(lastVisiblePosition >= linearLayoutManager.getItemCount() - 1){
                        Toast.makeText(Main.this,"上拉加载成功",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });*/
    }

    private String parseJSONWithJSONObject(String JsonData) {
        try {
            JSONArray jsonArray = new JSONArray(JsonData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JsonData;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.settings:
                if (!ButtonUtils.isFastDoubleClick(R.id.charge_up)) {
                    Intent settings = new Intent(Main.this, Main_Settings.class);
                    startActivity(settings);
                    return;
                }

                break;
            case R.id.home:
                if (!ButtonUtils.isFastDoubleClick(R.id.home)) {
                    NetBroadcastReceiver netBroadcastReceiver = new NetBroadcastReceiver();
                    boolean onnet = netBroadcastReceiver.isNetworkAvailable(getApplication());
                    if (onnet == true) {
                        SharedPreferences pref = getSharedPreferences("user", MODE_PRIVATE);
                        String username = pref.getString("name", "");

                        String number = A.api("http://139.199.70.192/zb/user.php?user=" + username + "");
                        family = A.api("http://139.199.70.192/zb/date_masses.php?masses=" + number + "");

                        Intent family = new Intent(Main.this, Main2.class);
                        startActivity(family);
                    }
                    else {
                        Toast.makeText(Main.this,"当前网络不可用",Toast.LENGTH_SHORT).show();
                    }
                }

                break;
            case R.id.charge_up:
                if (!ButtonUtils.isFastDoubleClick(R.id.charge_up)) {
                    NetBroadcastReceiver netBroadcastReceiver = new NetBroadcastReceiver();
                    boolean onnet = netBroadcastReceiver.isNetworkAvailable(getApplication());
                    if (onnet == true) {
                        Intent intent = new Intent(Main.this, Main_Record.class);
                        startActivity(intent);
                    }
                    else {
                        Toast.makeText(Main.this,"当前网络不可用",Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(Main.this, Main_Login.class);
            intent.putExtra(Main_Login.Main_EXIST, true);
            Main.this.finish();
            startActivity(intent);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


}
