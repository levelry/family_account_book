package com.example.talentry.family_book;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import me.imid.swipebacklayout.lib.app.SwipeBackActivity;

import static android.R.attr.onClick;

/**
 * Created by TalentRY on 2018/11/23.
 */

public class Main_Settings extends SwipeBackActivity implements View.OnClickListener {

    private RelativeLayout back;
    private TextView exit, update;
    private TextView test;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        findIDs();
        setListeners();
    }


    private void findIDs() {
        back = (RelativeLayout) findViewById(R.id.back);
        exit = (TextView) findViewById(R.id.exit);
        update = (TextView) findViewById(R.id.update);
        test = (TextView) findViewById(R.id.test);
    }


    private void setListeners() {
        back.setOnClickListener(this);
        exit.setOnClickListener(this);
        update.setOnClickListener(this);
        test.setOnClickListener(this);
    }

    public static String family;
   // public static String a;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.exit:
                new AlertDialog.Builder(Main_Settings.this).setTitle("提示").setMessage("是否退出登录？").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        Intent intent = new Intent(Main_Settings.this, Main_Login.class);
                        startActivity(intent);
                        finish();
                    }
                }).setNegativeButton("取消", null).show();
                break;
            case R.id.update:
                SharedPreferences pref = getSharedPreferences("user",MODE_PRIVATE);
                String username = pref.getString("name","");

                String number = A.api("http://139.199.70.192/zb/user.php?user="+username+"");
                family = A.api("http://139.199.70.192/zb/date_masses.php?masses="+number+"");

                //a = A.api("http://139.199.70.192/zb/get_date_user.php?user="+username);

                Intent intent = new Intent(Main_Settings.this, Main2.class);
                startActivity(intent);

                break;
            case R.id.test:
                ToastUtil.show(Main_Settings.this, "该功能正在开发");
                break;
        }
    }


}
