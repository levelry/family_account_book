package com.example.talentry.family_book;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by TalentRY on 2018/3/18.
 */

public class ToastUtil {
    public static Toast toast;

    public static void show(Context context, String showmessage) {
        if (toast == null) {
            toast = Toast.makeText(context, showmessage, Toast.LENGTH_SHORT);
        } else {
            toast.setText(showmessage);
        }
        LinearLayout linearLayout = (LinearLayout) toast.getView();
        TextView messageTextView = (TextView) linearLayout.getChildAt(0);
        messageTextView.setTextSize(15);
        toast.show();
    }

        /**
         * 取消Toast
         * onDestroy时调用，或者onPause
         * 当前页面finish之后在下一个页面不会显示
         */
        public static void cancelToast() {
            if (toast != null) {
                toast.cancel();
            }
        }

}
