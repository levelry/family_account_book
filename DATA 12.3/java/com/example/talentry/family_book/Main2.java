package com.example.talentry.family_book;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;

import java.util.List;

import static com.example.talentry.family_book.A.api;

/**
 * Created by TalentRY on 2018/11/21.
 */

public class Main2 extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout settings;
    private TextView expenditure,income;
    private List<List_Contact> data;
    private RecyclerView recyclerview;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        findIDs();
        setListeners();
        setData();

    }


    private void findIDs() {
        settings = (RelativeLayout) findViewById(R.id.settings);
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        expenditure = (TextView) findViewById(R.id.expenditure);
        income = (TextView) findViewById(R.id.income);
    }

    private void setListeners() {
        settings.setOnClickListener(this);
    }




    private void setData() {
        SharedPreferences pref = getSharedPreferences("user",MODE_PRIVATE);
        String username = pref.getString("name","");
        String number = A.api("http://139.199.70.192/zb/user.php?user="+username+"");
        int regist = Integer.parseInt(number);
        String zhichu = parseJSONWithJSONObject(api("http://139.199.70.192/zb/get_date_he_home.php?masses="+regist+"&money=1"));
        String shouru = parseJSONWithJSONObject(api("http://139.199.70.192/zb/get_date_he_home.php?masses="+regist+"&money=0"));
        expenditure.setText(zhichu);
        income.setText(shouru);


        List_Contacts2 contacts = new List_Contacts2();
        data = contacts.getContacts();

        List_Adapter adapter = new List_Adapter(data, this);
        recyclerview.setAdapter(adapter);


        LinearLayoutManager lin = new LinearLayoutManager(this);
        lin.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview.setLayoutManager(lin);

    }

    private String parseJSONWithJSONObject(String JsonData) {
        try
        {
            JSONArray jsonArray = new JSONArray(JsonData);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return JsonData;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }

   /* @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(Main2.this, Main_Login.class);
            intent.putExtra(Main_Login.Main_EXIST, true);
            Main2.this.finish();
            startActivity(intent);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }*/




}
