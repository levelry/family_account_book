package com.example.talentry.family_book;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by TalentRY on 2018/11/21.
 */

public class Main extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout settings;
    private TextView charge_up;
    private List<List_Contact> data;
    private RecyclerView recyclerview;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findIDs();
        setListeners();
        setData();
    }


    private void findIDs() {
        settings = (RelativeLayout) findViewById(R.id.settings);
        charge_up = (TextView) findViewById(R.id.charge_up);
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
    }

    private void setListeners() {
        settings.setOnClickListener(this);
        charge_up.setOnClickListener(this);
    }


    private void setData() {
        List_Contacts contacts = new List_Contacts();
        data = contacts.getContacts();

        List_Adapter adapter = new List_Adapter(data, this);
        recyclerview.setAdapter(adapter);


        LinearLayoutManager lin = new LinearLayoutManager(this);
        lin.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview.setLayoutManager(lin);


        /*recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            public LinearLayoutManager linearLayoutManager;
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == RecyclerView.SCROLL_STATE_IDLE){
                    int lastVisiblePosition = linearLayoutManager.findLastVisibleItemPosition();
                    if(lastVisiblePosition >= linearLayoutManager.getItemCount() - 1){
                        Toast.makeText(Main.this,"上拉加载成功",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });*/
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.settings:
                Intent settings = new Intent(Main.this, Main_Settings.class);
                startActivity(settings);
                break;
            case R.id.charge_up:
                if (!ButtonUtils.isFastDoubleClick(R.id.charge_up)) {
                    Intent intent = new Intent(Main.this, Main_Record.class);
                    startActivity(intent);
                    return;
                }
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(Main.this, Main_Login.class);
            intent.putExtra(Main_Login.EXIST, true);
            startActivity(intent);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public static final String EXIST = "exist";

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null) {//判断其他Activity启动本Activity时传递来的intent是否为空
            //获取intent中对应Tag的布尔值
            boolean isExist = intent.getBooleanExtra(EXIST, false);
            //如果为真则退出本Activity
            if (isExist) {
                this.finish();
            }
        }
    }


}
