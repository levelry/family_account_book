package com.example.talentry.family_book;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import me.imid.swipebacklayout.lib.app.SwipeBackActivity;
import zb.t;

/**
 * Created by TalentRY on 2018/11/20.
 */

public class Main_Modifypw extends SwipeBackActivity {

    private RelativeLayout back;
    private Button enter;
    private EditText username, userpassword,userpassword1, userpassword2,number;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());
        setContentView(R.layout.activity_modifypw);
        findIDs();
        setListener();
    }


    private void findIDs() {
        back = (RelativeLayout) findViewById(R.id.back);
        enter = (Button) findViewById(R.id.enter);
        username = (EditText) findViewById(R.id.username);
        userpassword = (EditText) findViewById(R.id.userpassword);
        userpassword1 = (EditText) findViewById(R.id.userpassword1);
        userpassword2 = (EditText) findViewById(R.id.userpassword2);
        number = (EditText) findViewById(R.id.number);
    }


    private void setListener() {
        /*返回键*/
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
         /*确认键*/
        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NetBroadcastReceiver netBroadcastReceiver = new NetBroadcastReceiver();
                boolean onnet = netBroadcastReceiver.isNetworkAvailable(getApplication());
                if (onnet == true) {
                    verification();/*注册验证*/
                }
                    else {
                    Toast.makeText(Main_Modifypw.this,"当前网络不可用",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


    /*注册验证*/
    private void verification() {
        String name = username.getText().toString();
        String password = userpassword.getText().toString();
        String password1 = userpassword1.getText().toString();
        String password2 = userpassword2.getText().toString();
        String num = number.getText().toString();
        String register = t.register(name, password1, password2);

       /* if (register.equals("密码错误")) {
            if (register.contains(name)) {
                Toast.makeText(Main_Register.this, "该用户名已被注册", Toast.LENGTH_LONG).show();
                return;
            }
        }*/

        if (TextUtils.isEmpty(name) || TextUtils.isEmpty(password) || TextUtils.isEmpty(password1) || TextUtils.isEmpty(password2)) {
            Toast.makeText(Main_Modifypw.this, "请检查您的输入", Toast.LENGTH_LONG).show();
            return;
        }
        if (password1.length() < 6) {
            Toast.makeText(Main_Modifypw.this, "密码过短(至少6位)", Toast.LENGTH_LONG).show();
            deled();
            return;
        }

        if (!TextUtils.equals(password1, password2)) {
            Toast.makeText(Main_Modifypw.this, "密码不一致", Toast.LENGTH_LONG).show();
            deled();
            return;
        }

        if (!TextUtils.isEmpty(num)){
            int regist = Integer.parseInt(num);
            A.api("http://139.199.70.192/zb/put_masses.php?user="+name+"&masses="+regist);

        }else {
            Toast.makeText(Main_Modifypw.this, "请检查您的输入", Toast.LENGTH_LONG).show();
            return;
        }

        if (register.equals("注册成功")) {
/*            SystemClock.sleep(1000);*/
            deled();
            Toast.makeText(Main_Modifypw.this, "注册成功", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(Main_Modifypw.this,Main_Login.class);
            startActivity(intent);
        } else {
            Toast.makeText(Main_Modifypw.this, register, Toast.LENGTH_LONG).show();
            deled();
            username.setText("");
            number.setText("");
        }
    }


    /*清空输入框*/
    private void deled() {
        userpassword1.setText("");
        userpassword2.setText("");
    }


}


