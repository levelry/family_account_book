package com.example.talentry.family_book;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import me.imid.swipebacklayout.lib.app.SwipeBackActivity;
import zb.t;

/**
 * Created by TalentRY on 2018/11/20.
 */

public class Main_Register extends SwipeBackActivity {

    private RelativeLayout back;
    private Button enter;
    private EditText username, userpassword1, userpassword2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());
        setContentView(R.layout.activity_register);
        findIDs();
        setListener();
    }


    private void findIDs() {
        back = (RelativeLayout) findViewById(R.id.back);
        enter = (Button) findViewById(R.id.enter);
        username = (EditText) findViewById(R.id.username);
        userpassword1 = (EditText) findViewById(R.id.userpassword1);
        userpassword2 = (EditText) findViewById(R.id.userpassword2);
    }


    private void setListener() {
        /*返回键*/
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
         /*确认键*/
        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NetBroadcastReceiver netBroadcastReceiver = new NetBroadcastReceiver();
                boolean onnet = netBroadcastReceiver.isNetworkAvailable(getApplication());
                if (onnet == true) {
                    verification();/*注册验证*/
                }
                    else {
                    Toast.makeText(Main_Register.this,"当前网络不可用",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


    /*注册验证*/
    private void verification() {
        String name = username.getText().toString();
        String password1 = userpassword1.getText().toString();
        String password2 = userpassword2.getText().toString();
        String register = zb.t.register(name, password1, password2);

       /* if (register.equals("密码错误")) {
            if (register.contains(name)) {
                Toast.makeText(Main_Register.this, "该用户名已被注册", Toast.LENGTH_LONG).show();
                return;
            }
        }*/

        if (TextUtils.isEmpty(name) || TextUtils.isEmpty(password1) || TextUtils.isEmpty(password2)) {
            Toast.makeText(Main_Register.this, "请检查您的输入", Toast.LENGTH_LONG).show();
            return;
        }
        if (!TextUtils.equals(password1, password2)) {
            Toast.makeText(Main_Register.this, "密码不一致", Toast.LENGTH_LONG).show();
            deled();
            return;
        }
        if (password1.length() < 6) {
            Toast.makeText(Main_Register.this, "密码过短(至少6位)", Toast.LENGTH_LONG).show();
            deled();
            return;
        }

        if (register.equals("注册成功")) {
/*            SystemClock.sleep(1000);*/
            Toast.makeText(Main_Register.this, "注册成功", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            Toast.makeText(Main_Register.this, register, Toast.LENGTH_LONG).show();
            deled();
            username.setText("");
        }


    }


    /*清空输入框*/
    private void deled() {
        userpassword1.setText("");
        userpassword2.setText("");
    }


}


