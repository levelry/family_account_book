package com.example.talentry.family_book;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import zb.t;

public class Main_Login extends AppCompatActivity implements View.OnClickListener {

    NetBroadcastReceiver netWorkStateReceiver;
    private TextView register;
    private Button login;
    private EditText username, userpassword;
    private Button test;
    private ImageView eye;
    private int flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());
        setContentView(R.layout.activity_login);
        findIDs();
        set_eyes();
        setListeners();
    }

    private void set_eyes() {
        userpassword.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String pwd = userpassword.getText().toString();
                if (TextUtils.isEmpty(pwd)) {
                    eye.setVisibility(View.GONE);
                } else {
                    eye.setVisibility(View.VISIBLE);
                }
            }
        });
    }


    private void findIDs() {
        test = (Button) findViewById(R.id.test);
        eye = (ImageView) findViewById(R.id.eye);
        register = (TextView) findViewById(R.id.register);
        login = (Button) findViewById(R.id.login);
        username = (EditText) findViewById(R.id.username);
        userpassword = (EditText) findViewById(R.id.userpassword);
    }

    private void setListeners() {
        test.setOnClickListener(this);
        eye.setOnClickListener(this);
        register.setOnClickListener(this);
        login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.test:
                if (!ButtonUtils.isFastDoubleClick(R.id.test)) {
                    Intent test = new Intent(Main_Login.this, Main.class);
                    startActivity(test);
                    return;
                }
                break;
            case R.id.eye:
                switch (flag) {
                    case 0:
                        userpassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                        userpassword.setSelection(userpassword.getText().toString().length());
                        eye.setImageResource(R.drawable.eye2);
                        flag = 1;
                        return;
                    case 1:
                        userpassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        userpassword.setSelection(userpassword.getText().toString().length());
                        eye.setImageResource(R.drawable.eye1);
                        flag = 0;
                        return;
                }
                break;
            case R.id.register:
                Intent register = new Intent(Main_Login.this, Main_Register.class);
                startActivity(register);
                break;
            case R.id.login:
                NetBroadcastReceiver netBroadcastReceiver = new NetBroadcastReceiver();
                boolean onnet = netBroadcastReceiver.isNetworkAvailable(getApplication());
                if (onnet == true) {
                    String name = username.getText().toString();
                    String password = userpassword.getText().toString();
                    if (TextUtils.isEmpty(name) || TextUtils.isEmpty(password)) {
                        Toast.makeText(Main_Login.this, "请输入正确的用户名和密码", Toast.LENGTH_LONG).show();
                    } else {
                        String islogin = zb.t.login(name, password);
                        if (islogin.equals("登录成功")) {
                            Intent intent = new Intent(Main_Login.this, Main.class);
                            startActivity(intent);
                            return;
                        }
                        if (islogin.equals("密码错误")) {
                            Toast.makeText(Main_Login.this, "密码错误", Toast.LENGTH_SHORT).show();
                            userpassword.setText("");
                        } else {
                            Toast.makeText(Main_Login.this, islogin, Toast.LENGTH_LONG).show();
                            delet();
                        }
                    }
                }else {
                    Toast.makeText(Main_Login.this,"当前网络不可用",Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void delet() {
        userpassword.setText("");
        username.setText("");
    }


    public static final String Main_EXIST = "exist";
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null) {//判断其他Activity启动本Activity时传递来的intent是否为空
            //获取intent中对应Tag的布尔值
            boolean isExist = intent.getBooleanExtra(Main_EXIST, false);
            //如果为真则退出本Activity
            if (isExist) {
                this.finish();
            }
        }
    }

    //在onResume()方法注册
    @Override
    protected void onResume() {
        if (netWorkStateReceiver == null) {
            netWorkStateReceiver = new NetBroadcastReceiver();
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(netWorkStateReceiver, filter);
        System.out.println("注册");
        super.onResume();
    }

    //onPause()方法注销
    @Override
    protected void onPause() {
        unregisterReceiver(netWorkStateReceiver);
        System.out.println("注销");
        super.onPause();
    }


}
