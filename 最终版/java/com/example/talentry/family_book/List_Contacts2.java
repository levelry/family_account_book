package com.example.talentry.family_book;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.example.talentry.family_book.A.api;

/**
 * Created by TalentRY on 2018/9/26.
 */

public class List_Contacts2 extends AppCompatActivity{

    private List<List_Contact> contacts = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }



    public List_Contacts2() {
        parseJSONWithJSONObject(Main.family);
    }



    private void parseJSONWithJSONObject(String JsonData) {
        try
        {
            JSONArray jsonArray = new JSONArray(JsonData);
            for (int i=0; i < jsonArray.length(); i++)    {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String user = jsonObject.getString("user");
                String source = jsonObject.getString("source");
                String money = jsonObject.getString("money");
                contacts.add(new List_Contact(user,source,money));
            }
        }
        catch (Exception e)
        {
            Toast.makeText(this, "出错", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }


    public List<List_Contact> getContacts() {
        return contacts;
    }

}
