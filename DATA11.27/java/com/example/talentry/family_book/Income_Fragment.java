package com.example.talentry.family_book;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.text.DecimalFormat;


/**
 * Created by TalentRY on 2018/11/21.
 */

public class Income_Fragment extends Fragment implements View.OnClickListener {

    private EditText income;
    private EditText remarks;
    private Button enter;
    private View view;
    private ImageView image1,image2,image3,image4;
    private int flag1,flag2,flag3,flag4;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_income, container, false);
        findIDs();
        setNumber();
        setListeners();
        return view;
    }


    private void findIDs() {
        remarks = view.findViewById(R.id.remarks);
        income = view.findViewById(R.id.income);
        enter = view.findViewById(R.id.enter);
        image1 = view.findViewById(R.id.image1);
        image2 = view.findViewById(R.id.image2);
        image3 = view.findViewById(R.id.image3);
        image4 = view.findViewById(R.id.image4);
    }

    private void setNumber() {
        income.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String temp = editable.toString();

                int posision = temp.indexOf(".");
                if (posision < 0) {
                    return;
                }
                if (posision == 0) {
                    editable.insert(0, "0");
                }
                if (temp.length() - posision - 1 > 2) {
                    editable.delete(posision + 3, posision + 4);
                }
            }
        });
    }


    private void setListeners() {
        enter.setOnClickListener(this);
        image1.setOnClickListener(this);
        image2.setOnClickListener(this);
        image3.setOnClickListener(this);
        image4.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image1:
                switch (flag1) {
                    case 0:
                        image1.setImageResource(R.drawable.f2);
                        flag1 = 1;
                        return;
                    case 1:
                        image1.setImageResource(R.drawable.f0);
                        flag1 = 0;
                        return;
                }
                break;
            case R.id.image2:
                switch (flag2) {
                    case 0:
                        image2.setImageResource(R.drawable.c2);
                        flag2 = 1;
                        return;
                    case 1:
                        image2.setImageResource(R.drawable.c0);
                        flag2 = 0;
                        return;
                }
                break;
            case R.id.image3:
                switch (flag3) {
                    case 0:
                        image3.setImageResource(R.drawable.t2);
                        flag3 = 1;
                        return;
                    case 1:
                        image3.setImageResource(R.drawable.t0);
                        flag3 = 0;
                        return;
                }
                break;
            case R.id.image4:
                switch (flag4) {
                    case 0:
                        image4.setImageResource(R.drawable.o2);
                        flag4 = 1;
                        return;
                    case 1:
                        image4.setImageResource(R.drawable.o0);
                        flag4 = 0;
                        return;
                }
                break;

            case R.id.enter:
                String aa = income.getText().toString();

                String remark = remarks.getText().toString();
                if (TextUtils.isEmpty(remark)||TextUtils.isEmpty(aa)){
                    Toast.makeText(getActivity(), "请检查输入的内容", Toast.LENGTH_SHORT).show();
                }else {
                    float i = Float.parseFloat(aa);
                    DecimalFormat dnf = new DecimalFormat("0.00");
                    String newNumber = dnf.format(i);
                    Toast.makeText(getActivity(), "金额为" + newNumber +"\n"+"备注为" + remark, Toast.LENGTH_SHORT).show();

                    income.setText("");
                }

/*
                String aa = income.getText().toString();
                boolean status = aa.contains(".");
                if(status){
                    System.out.println("包含");
                    int i = Integer.parseInt(aa);
                    DecimalFormat dnf=new DecimalFormat ("0.00");
                    String newNumber=dnf.format(i);
                    Toast.makeText(getActivity(), newNumber, Toast.LENGTH_SHORT).show();
                }else{
                    int i = Integer.parseInt(aa);
                    DecimalFormat dnf=new DecimalFormat ("0.00");
                    String newNumber=dnf.format(i);
                    Toast.makeText(getActivity(), newNumber, Toast.LENGTH_SHORT).show();
                }*/
                break;
        }
    }
}
