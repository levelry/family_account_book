package com.example.talentry.family_book;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.example.talentry.family_book.A.api;

/**
 * Created by TalentRY on 2018/9/26.
 */

public class List_Contacts extends AppCompatActivity{

    private List<List_Contact> contacts = new ArrayList<>();


    public List_Contacts() {
        parseJSONWithJSONObject(api("http://139.199.70.192/zb/get_date_user.php?user="+Main_Login.usname));
    }



    private void parseJSONWithJSONObject(String JsonData) {
        try
        {
            JSONArray jsonArray = new JSONArray(JsonData);
            for (int i=0; i < jsonArray.length(); i++)    {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String user = jsonObject.getString("user");
                String source = jsonObject.getString("source");
                String money = jsonObject.getString("money");
                contacts.add(new List_Contact(user,source,money));
            }
        }
        catch (Exception e)
        {
            Toast.makeText(this, "出错", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }


    public List<List_Contact> getContacts() {
        return contacts;
    }

}
