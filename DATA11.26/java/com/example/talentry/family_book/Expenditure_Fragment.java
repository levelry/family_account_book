package com.example.talentry.family_book;


import android.icu.text.SimpleDateFormat;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.Date;

/**
 * Created by TalentRY on 2018/11/21.
 */

public class Expenditure_Fragment extends Fragment implements View.OnClickListener {

    private EditText remarks;
    private EditText expenditure;
    private Button enter;
    private View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_expenditure, container, false);
        findIDs();
        setNumber();
        setListeners();
        return view;
    }


    private void findIDs() {
        remarks = view.findViewById(R.id.remarks);
        expenditure = view.findViewById(R.id.expenditure);
        enter = view.findViewById(R.id.enter);
    }

    private void setNumber() {
        expenditure.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                DecimalFormat df = new DecimalFormat("#.00");
                String temp = editable.toString();
                int posision = temp.indexOf(".");
                if (posision < 0) {
                    return;
                }
                if (posision == 0) {
                    editable.insert(0, "0");
                }
                if (temp.length() - posision - 1 > 2) {
                    editable.delete(posision + 3, posision + 4);
                }


            }
        });
    }

    private void setListeners() {
        enter.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.enter:
                String remark = remarks.getText().toString();
                String aa = expenditure.getText().toString();
                /*if (TextUtils.isEmpty(remark)||TextUtils.isEmpty(aa)){
                    Toast.makeText(getActivity(), "请检查输入的内容", Toast.LENGTH_SHORT).show();
                }else {
                    float i = Float.parseFloat(aa);
                    DecimalFormat dnf = new DecimalFormat("0.00");
                    String newNumber = dnf.format(i);
                    Toast.makeText(getActivity(), "金额为" + newNumber +"\n"+"备注为" + remark, Toast.LENGTH_SHORT).show();

                    expenditure.setText("");
                }
*/

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");// HH:mm:ss
                //获取当前时间
                Date date = new Date(System.currentTimeMillis());
                Toast.makeText(getActivity(), simpleDateFormat.format(date), Toast.LENGTH_SHORT).show();
        }
    }
}
