package com.example.talentry.family_book;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputConnectionWrapper;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import zb.t;

public class Main_Login extends AppCompatActivity implements View.OnClickListener {

    private TextView register;
    private Button login;
    private EditText username, userpassword;
    private Button test;
    private ImageView eye;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());
        setContentView(R.layout.activity_login);
        findIDs();
        isNetworkAvailable(getApplicationContext());

    }


    private void findIDs() {
        test = (Button) findViewById(R.id.test);
        eye = (ImageView) findViewById(R.id.eye);
        register = (TextView) findViewById(R.id.register);
        login = (Button) findViewById(R.id.login);
        username = (EditText) findViewById(R.id.username);
        userpassword = (EditText) findViewById(R.id.userpassword);
    }


    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getActiveNetworkInfo();
            if (info != null && info.isConnected()) {
                if (info.getState() == NetworkInfo.State.CONNECTED) {
                    setListeners();
                    return true;
                }
            } else {
                Toast.makeText(Main_Login.this, "当前网络不可用", Toast.LENGTH_SHORT).show();
                notNetwork();
                return false;
            }
        }
        return false;
    }

    private void notNetwork() {
        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Main_Login.this, Main.class);
                startActivity(intent);
                Main_Login.this.finish();
                delet();
            }
        });
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(Main_Login.this, "当前网络不可用", Toast.LENGTH_SHORT).show();
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(Main_Login.this, "当前网络不可用", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void setListeners() {
        test.setOnClickListener(this);
        register.setOnClickListener(this);
        login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.test:
                if (!ButtonUtils.isFastDoubleClick(R.id.test)) {
                    Intent test = new Intent(Main_Login.this, Main.class);
                    startActivity(test);
                    return;
                }
                break;
            case R.id.eye:
                /*userpassword.setOnClickListener();*/
                break;
            case R.id.register:
                Intent register = new Intent(Main_Login.this, Main_Register.class);
                startActivity(register);
                break;
            case R.id.login:
                String name = username.getText().toString();
                String password = userpassword.getText().toString();
                if (TextUtils.isEmpty(name) || TextUtils.isEmpty(password)) {
                    Toast.makeText(Main_Login.this, "请输入正确的用户名和密码", Toast.LENGTH_LONG).show();
                    return;
                } else {
                    String islogin = zb.t.login(name, password);
                    if (islogin.equals("登录成功")) {
                        Intent intent = new Intent(Main_Login.this, Main.class);
                        startActivity(intent);
                        Main_Login.this.finish();
                        delet();
                        return;
                    }
                    if (islogin.equals("密码错误")) {
                        Toast.makeText(Main_Login.this, "密码错误", Toast.LENGTH_SHORT).show();
                        userpassword.setText("");
                        return;
                    } else {
                        Toast.makeText(Main_Login.this, islogin, Toast.LENGTH_LONG).show();
                        delet();
                        return;
                    }
                }
        }
    }

    private void delet() {
        username.setText("");
        userpassword.setText("");
    }


    public static final String EXIST = "exist";

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null) {//判断其他Activity启动本Activity时传递来的intent是否为空
            //获取intent中对应Tag的布尔值
            boolean isExist = intent.getBooleanExtra(EXIST, false);
            //如果为真则退出本Activity
            if (isExist) {
                this.finish();
            }
        }
    }


}
