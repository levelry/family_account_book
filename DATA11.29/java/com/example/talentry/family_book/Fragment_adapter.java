package com.example.talentry.family_book;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by TalentRY on 2018/9/12.
 */

// Fragment适配器
public class Fragment_adapter extends FragmentPagerAdapter {
    private List<Fragment> list;

    public Fragment_adapter(FragmentManager fragmentManager, List<Fragment> list) {
        super(fragmentManager);
        this.list = list;
    }

    @Override
    public Fragment getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getCount() {
        return list.size();
    }
}
