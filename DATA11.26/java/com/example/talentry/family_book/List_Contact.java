package com.example.talentry.family_book;

import java.io.Serializable;

/**
 * Created by TalentRY on 2018/9/26.
 */

public class List_Contact implements Serializable {

    private String classify;
    private String date;
    private String price;

    public List_Contact(String classify, String date, String price) {
        this.classify = classify;
        this.date = date;
        this.price = price;
    }

    public String getClassify() {
        return classify;
    }

    public String getDate() {
        return date;
    }

    public String getPrice() {
        return price;
    }
}
