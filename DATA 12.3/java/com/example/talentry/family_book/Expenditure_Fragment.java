package com.example.talentry.family_book;


import android.content.Intent;
import android.content.SharedPreferences;
import android.icu.text.SimpleDateFormat;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Date;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by TalentRY on 2018/11/21.
 */

public class Expenditure_Fragment extends Fragment implements View.OnClickListener {

    private EditText remarks;
    private EditText expenditure;
    private Button enter;
    private View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_expenditure, container, false);
        findIDs();
        setNumber();
        setListeners();
        return view;
    }


    private void findIDs() {
        remarks = view.findViewById(R.id.remarks);
        expenditure = view.findViewById(R.id.expenditure);
        enter = view.findViewById(R.id.enter);
    }

    private void setNumber() {
        expenditure.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                DecimalFormat df = new DecimalFormat("#.00");
                String temp = editable.toString();
                int posision = temp.indexOf(".");
                if (posision < 0) {
                    return;
                }
                if (posision == 0) {
                    editable.insert(0, "0");
                }
                if (temp.length() - posision - 1 > 2) {
                    editable.delete(posision + 3, posision + 4);
                }


            }
        });
    }

    private void setListeners() {
        enter.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.enter:
                String remark = remarks.getText().toString();
                String expend = expenditure.getText().toString();
                if (TextUtils.isEmpty(remark)||TextUtils.isEmpty(expend)){
                    Toast.makeText(getActivity(), "请检查输入的内容", Toast.LENGTH_SHORT).show();
                }else {
                    float value = Float.valueOf(expend.toString());
                    float b = -(Math.abs(value));
                    SharedPreferences pref = this.getActivity().getSharedPreferences("user",MODE_PRIVATE);
                    String username = pref.getString("name","");
                    A.api("http://139.199.70.192/zb/put_date.php?user="+username+"&source="+remark+"&money="+b);
                    Intent intent = new Intent(getActivity(), Main.class);
                    startActivity(intent);
                   /* float i = Float.parseFloat(aa);
                    DecimalFormat dnf = new DecimalFormat("0.00");
                    String newNumber = dnf.format(i);
                    Toast.makeText(getActivity(), "金额为" + newNumber +"\n"+"备注为" + remark, Toast.LENGTH_SHORT).show();*/
                }


        }
    }




}
