package com.example.talentry.family_book;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by TalentRY on 2018/3/18.
 */

public class ToastUtil {
    public static Toast toast;

    public static void show(Context context, String showmessage) {
        if (toast == null) {
            toast = Toast.makeText(context, showmessage, Toast.LENGTH_SHORT);
        } else {
            toast.setText(showmessage);
        }
        toast.show();
    }
}
