package com.example.talentry.family_book;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by TalentRY on 2018/10/12.
 */

public class List_Adapter extends RecyclerView.Adapter<List_Adapter.MyAdapter> {

    private List<List_Contact> contacts;
    private Context context;

    public List_Adapter(List<List_Contact> contacts, Context context) {
        this.contacts = contacts;
        this.context = context;
    }

    @Override
    public MyAdapter onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.activity_listdiy, parent, false);
        return new MyAdapter(view);
    }

    @Override
    public void onBindViewHolder(MyAdapter holder, int position) {
        List_Contact contact = contacts.get(position);
        holder.classify.setText(contact.getClassify());
        holder.date.setText(contact.getDate());
        holder.price.setText(contact.getPrice());
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }





    public class MyAdapter extends RecyclerView.ViewHolder {
        private View view;
        private TextView classify, date, price;

        public MyAdapter(View itemView) {
            super(itemView);
            view = itemView;
            classify = view.findViewById(R.id.classify);
            date = view.findViewById(R.id.date);
            price = view.findViewById(R.id.price);
        }
    }

}
