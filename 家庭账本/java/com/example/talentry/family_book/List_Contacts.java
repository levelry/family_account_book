package com.example.talentry.family_book;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TalentRY on 2018/9/26.
 */

public class List_Contacts {

    private List<List_Contact> contacts = new ArrayList<>();

    public List_Contacts() {
        contacts.add(new List_Contact("食品", "2018.11.23", "+", "19.00"));
        contacts.add(new List_Contact("生活用品", "2018.12.24", "+", "29.10"));
        contacts.add(new List_Contact("理发", "2018.01.25", "+", "39.10"));
        contacts.add(new List_Contact("购物", "2018.02.26", "+", "49.20"));
        contacts.add(new List_Contact("生活用品", "2018.03.27", "+", "59.40"));
        contacts.add(new List_Contact("理发", "2018.04.28", "+", "69.50"));
        contacts.add(new List_Contact("购物", "2018.05.29", "+", "79.60"));
        contacts.add(new List_Contact("食品", "2018.06.30", "+", "89.70"));
        contacts.add(new List_Contact("生活用品", "2018.07.01", "+", "99.80"));
        contacts.add(new List_Contact("购物", "2018.08.02", "+", "109.90"));
        contacts.add(new List_Contact("理发", "2018.09.03", "+", "119.00"));
    }


    public List<List_Contact> getContacts() {
        return contacts;
    }

}
